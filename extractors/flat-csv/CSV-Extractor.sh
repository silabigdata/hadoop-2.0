#!/usr/bin/env bash
source config/config.cfg

if [ -r "$1" ]
then
  echo "$1 is readable"
else
  echo "$1 is not readable"
fi

fileName=$1
headerLines=$2

echo "Processing file : $fileName"
echo "Number of header lines : $headerLines"

#Create Table if not exists
./helper-scripts/createStagingTable.sh $headerLines
#Need to figure out where to put the file on HDFS
hdfs dfs -put $fileName $tablePath
#The sparkScipt contains the path to the Scala Spark Script which will provide the transformation
spark-shell -i $sparkScript
#todo: Load the output from the transformation into the target DIL table
