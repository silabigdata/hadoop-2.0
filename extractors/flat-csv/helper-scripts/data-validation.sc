//may need to use the two below configs:
//hiveContext.setConf("hive.exec.dynamic.partition", "true")
//hiveContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

val ssc = new org.apache.spark.sql.SQLContext(sc)
   ssc = new SQLContext(sc)
   val tempTable = ssc.sql("SELECT
   ROW_NUMBER OVER(order by 1) AS BATCH_ROW_NUM,
   CURRENT_DATE AS BATCH_INS_DATE,
   //todo: get parameter as INS_BATCH_ID,
   //todo: get parameter as LAST_SOURCE_FILENAME,
   CAST(s_TYPE as VARCHAR(8)) AS TYPE,
   CAST(s_CHARGING_ID as BIGINT) AS CHARGING_ID,
   CAST(s_PGW_SEQ as BIGINT) AS PGW_SEQ,
   CAST(s_CONTAINER_SEQ as BIGINT) as CONTAINER_SEQ,
   CAST(s_RECORD_SEQ as BIGINT) as RECORD_SEQ,
   CAST(s_SGW_PLMN as VARCHAR(6)) as SGW_PLMN,
   CAST(s_PGW_PLMN as VARCHAR(6)) as PGW_PLMN,
   CAST(s_IMSI_T as VARCHAR(16)) as IMSI_T,
   CAST(s_IMEI_SV as VARCHAR(16)) as IMEI_SV,
   CAST(s_MSISDN as VARCHAR(15)) as MSISDN,
   CAST(s_RAT as TINYINT) as RAT,
   CAST(s_APN as VARCHAR(64)) as APN,
   CAST(s_PGW as VARCHAR(64)) as PGW,
   CAST(s_PGW_IP as VARCHAR(15)) as PGW_IP,
   CAST(s_SGW_IP as VARCHAR(15)) as SGW_IP,
   CAST(s_CLIENT_IP as VARCHAR(64)) as CLIENT_IP,
   CAST(s_CDR_START as VARCHAR(14)) as CDR_START,
   CAST(s_CDR_STOP as VARCHAR(14)) as CDR_STOP,
   CAST(s_CONTAINER_START as VARCHAR(14)) as CONTAINER_START,
   CAST(s_CONTAINER_STOP as VARCHAR(14)) as CONTAINER_STOP,
   CAST(s_UL_VOLUME as BIGINT) as UL_VOLUME,
   CAST(s_DL_VOLUME as BIGINT) as DL_VOLUME,
   CAST(s_REASON as TINYINT) as REASON,
   CAST(s_CHARGING as INT) as CHARGING,
   CAST(s_RATING_GROUP as TINYINT) as RATING_GROUP,
   CAST(s_RATING_QOS as TINYINT) as RATING_QOS,
   CAST(s_PDN_CONNECTION_ID as BIGINT) as PDN_CONNECTION_ID,
   CAST(s_ULI_CGI_PLMN as VARCHAR(6)) as ULI_CGI_PLMN,
   CAST(s_ULI_CGI_LAC as VARCHAR(4)) as ULI_CGI_LAC,
   CAST(s_ULI_CGI_CI as VARCHAR(5)) as ULI_CGI_CI,
   CAST(s_ULI_SAI_PLMN as VARCHAR(6)) as ULI_SAI_PLMN,
   CAST(s_ULI_SAI_LAC as VARCHAR(4)) as ULI_SAI_LAC,
   CAST(s_ULI_SAI_SAC as VARCHAR(4)) as ULI_SAI_SAC,
   CAST(s_ULI_RAI_PLMN as VARCHAR(6)) as ULI_RAI_PLMN,
   CAST(s_ULI_RAI_LAC as VARCHAR(4)) as ULI_RAI_LAC,
   CAST(s_ULI_RAI_RAC as VARCHAR(4)) as ULI_RAI_RAC,
   CAST(s_ULI_TAI_PLMN as VARCHAR(6)) as ULI_TAI_PLMN,
   CAST(s_ULI_TAI_TAC as VARCHAR(4)) as ULI_TAI_TAC,
   CAST(s_ULI_ECGI_PLMN as VARCHAR(6)) as ULI_ECGI_PLMN,
   CAST(s_ULI_ECGI_ECI as VARCHAR(32)) as ULI_ECGI_ECI,
   CAST(s_ULI_LAI_PLMN as VARCHAR(6)) as ULI_LAI_PLMN,
   CAST(s_ULI_LAI_LAC as VARCHAR(4)) as ULI_LAI_LAC,
   CAST(s_PP3G_USER_LOCATION_INFO as VARCHAR(250)) as PP3G_USER_LOCATION_INFO,
   CAST(s_USER_LOC_TAI as VARCHAR(20)) as USER_LOC_TAI,
   CAST(s_AF_CHARGING_ID as VARCHAR(32)) as AF_CHARGING_ID,
   CAST(s_ULI_ECGI_ECI_ADDITIONAL_INFO as VARCHAR(250)) as ULI_ECGI_ECI_ADDITIONAL_INFO)
   FROM staging.LTE_ROAM_DATA_USAGE").collect

   //TODO: Go through each column that needs to be validated and append it to a
   //      bad entries dataframe.

    //TODO: do a filter that removes all of the values that have declared as NULL or BAD
    //      if we can filter based on the ROW_NUMBER, that should be able to remove all of the values


    tempTable.cache

    //can use the row.isNullAt() to do null checks

    //Need to first NULL check each collumn

    val badEntries = tempTable.filter("todo: Insert SQL filter function to get bad results")

    val goodEntries = tempTable.filter("todo: Insert SQL filter function to get good results")

    val numInserted = goodEntries.count
    val numBadRecords = badEntries.count

    badEntries.write.text("/path/to/file")
    goodEntries.write.mode(SaveMode.Append).paritionBy('column','name').insertInto('tableName')

 System.quit(0)
