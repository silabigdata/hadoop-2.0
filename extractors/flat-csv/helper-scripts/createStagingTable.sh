#This script is used to build the LTE_DATA Staging table
#Initiallly developed on 8/23/2016 by Eric Simon

#Inputs
#1 - The number of the rows that are header part of the header
hive -e "CREATE EXTERNAL TABLE IF NOT EXISTS Staging.LTE_ROAM_DATA_USAGE
(
  s_TYPE                          STRING,
  s_CHARGING_ID                   STRING,
  s_PGW_SEQ                       STRING,
  s_CONTAINER_SEQ                 STRING,
  s_RECORD_SEQ                    STRING,
  s_SGW_PLMN                      STRING,
  s_PGW_PLMN                      STRING,
  s_IMSI_T                        STRING,
  s_IMEI_SV                       STRING,
  s_MSISDN                        STRING,
  s_RAT                           STRING,
  s_APN                           STRING,
  s_PGW                           STRING,
  s_PGW_IP                        STRING,
  s_SGW_IP                        STRING,
  s_CLIENT_IP                     STRING,
  s_CDR_START                     STRING,
  s_CDR_STOP                      STRING,
  s_CONTAINER_START               STRING,
  s_CONTAINER_STOP                STRING,
  s_UL_VOLUME                     STRING,
  s_DL_VOLUME                     STRING,
  s_REASON                        STRING,
  s_CHARGING                      STRING,
  s_RATING_GROUP                  STRING,
  s_RATING_QOS                    STRING,
  s_PDN_CONNECTION_ID             STRING,
  s_ULI_CGI_PLMN                  STRING,
  s_ULI_CGI_LAC                   STRING,
  s_ULI_CGI_CI                    STRING,
  s_ULI_SAI_PLMN                  STRING,
  s_ULI_SAI_LAC                   STRING,
  s_ULI_SAI_SAC                   STRING,
  s_ULI_RAI_PLMN                  STRING,
  s_ULI_RAI_LAC                   STRING,
  s_ULI_RAI_RAC                   STRING,
  s_ULI_TAI_PLMN                  STRING,
  s_ULI_TAI_TAC                   STRING,
  s_ULI_ECGI_ECI                  STRING,
  s_ULI_ECGI_PLMN                 STRING,
  s_ULI_LAI_PLMN                  STRING,
  s_ULI_LAI_LAC                   STRING,
  s_PP3G_USER_LOCATION_INFO       STRING,
  s_USER_LOC_TAI                  STRING,
  s_AF_CHARGING_ID                STRING,
  s_ULI_ECGI_ECI_ADDITIONAL_INFO  STRING
)
COMMENT 'This is LTE roaming data usage staging table'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
STORED AS TEXTFILE
location '/user/cloudera/staging/LTE_DATA'
tblproperties ('skip.header.line.count'=$1);"
